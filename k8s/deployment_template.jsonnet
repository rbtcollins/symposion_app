function (slug, sha) {
  
  local app = "symposion-app-" + slug,
  local namespace = "rego-review",
  local domain = slug + ".dev.lca2019.org",
  
  "deployment.json":
    {
      "apiVersion": "v1",
      "items": [
        {
          "apiVersion": "v1",
          "kind": "Service",
          "metadata": {
            "labels": {
              "app": "symposion-app",
              "slug": slug
            },
            "name": app,
            "namespace": namespace
          },
          "spec": {
            "ports": [
              {
                "port": 8000,
                "protocol": "TCP",
                "targetPort": 8000
              }
            ],
            "selector": {
              "app": "symposion-app",
              "slug": slug
            },
            "sessionAffinity": "None",
            "type": "ClusterIP"
          }
        },
        {
          "apiVersion": "extensions/v1beta1",
          "kind": "Deployment",
          "metadata": {
            "labels": {
              "app": "symposion-app",
              "slug": slug,
            },
            "name": app,
            "namespace": namespace
          },
          "spec": {
            "replicas": 1,
            "revisionHistoryLimit": 1,
            "selector": {
              "matchLabels": {
                "app": "symposion-app",
                "slug": slug
              }
            },
            "strategy": {
              "rollingUpdate": {
                "maxSurge": 1,
                "maxUnavailable": 1
              },
              "type": "RollingUpdate"
            },
            "template": {
              "metadata": {
                "labels": {
                  "app": "symposion-app",
                  "slug": slug
                }
              },
              "spec": {
                "containers": [
                  {
                    "env": [
                      {
                        "name": "SYMPOSION_APP_DEBUG",
                        "value": "1"
                      },
                      {
                        "name": "DJANGO_SECRET_KEY",
                        "value": "paGhahQuain5ohYoh0moorai"
                      },
                      {
                        "name": "DATABASE_URL",
                        "value": "sqlite:////tmp/symposion.sqlite"
                      },
                      {
                        "name": "GCS_BUCKET",
                        "value": "CEA51A5-A613-4AEF-A9FB-D0A57D77C13B"
                      },
                      {
                        "name": "GOOGLE_APPLICATION_CREDENTIALS",
                        "value": "/dev/null"
                      },
                      {
                        "name": "STRIPE_PUBLIC_KEY",
                        "value": "5CEA51A5-A613-4AEF-A9FB-D0A57D77C13B"
                      },
                      {
                        "name": "STRIPE_SECRET_KEY",
                        "value": "5CEA51A5-A613-4AEF-A9FB-D0A57D77C13B"
                      },
                      {
                        "name": "SYMPOSION_DEV_MODE",
                        "value": "LAPTOP"
                      },
                      {
                        "name": "ANALYTICS_KEY",
                        "value": "UA-000000000-1"
                      }
                    ],
                    "image": "asia.gcr.io/linuxconfsydney/symposion_app_2019_dev:" + sha,
                    "imagePullPolicy": "Always",
                    "livenessProbe": {
                      "failureThreshold": 3,
                      "httpGet": {
                        "path": "/admin/login/",
                        "port": 8000,
                        "scheme": "HTTP"
                      },
                      "initialDelaySeconds": 120,
                      "periodSeconds": 10,
                      "successThreshold": 1,
                      "timeoutSeconds": 2
                    },
                    "name": "symposion-app",
                    "ports": [
                      {
                        "containerPort": 8000,
                        "protocol": "TCP"
                      }
                    ],
                    "resources": {},
                    "terminationMessagePath": "/dev/termination-log",
                    "terminationMessagePolicy": "File"
                  }
                ],
                "dnsPolicy": "ClusterFirst",
                "restartPolicy": "Always",
                "schedulerName": "default-scheduler",
                "securityContext": {},
                "terminationGracePeriodSeconds": 30
              }
            }
          }
        }
      ],
      "kind": "List"
    },
  "ingress-patch.json": [
      {
        "op": "add",
        "path": "/spec/rules/-",
        "value": {
          "host": domain,
          "http": {
            "paths": [
              {
                "backend": {
                  "serviceName": app,
                  "servicePort": 8000
                },
                "path": "/"
              }
            ]
          }
        }
      },
      {
        "op": "add",
        "path": "/spec/tls/0/hosts/-",
        "value": domain
      }
  ],
   "certificate-patch.json": [
      {
        "op": "add",
        "path": "/spec/acme/config/-",
        "value": {
          "domains": [ domain ],
          "http01": {
            "ingress": "",
            "ingressClass": "nginx-review"
          }
        }
      },
      {
        "op": "add",
        "path": "/spec/dnsNames/-",
        "value": domain
      }
  ]
}
